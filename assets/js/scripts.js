jQuery(document).ready(function() {
	/*jQuery(document).on('change', '.gi-prod-qty input', function(){
		fie_quantity = jQuery(this).val();
		fie_product_id = jQuery(this).parent().parent().data('prod-id');
		fie_variation_id = jQuery(this).parent().parent().data('variation-id');

		var fie_gallery_data = jQuery('.fw-area').data('clicked-icon').split('-');
		fie_gallery_item_id = fie_gallery_data[1];

		var fie_use = fie_get_use(fie_product_id, fie_enabled);
		if (fie_quantity > 0 && fie_use > 0) {
			fie_process_image_editor(fie_use);
		}
	});*/
	jQuery('.fie-popup .fie-launch').click(function(){
		jQuery('.fie-popup .fie-popup-actions').hide();
		jQuery('.fie-popup .fie-popup-process').show();
		fie_goto_editor();
	});
	jQuery('.fie-popup .fie-use-asis').click(function(){
		jQuery.colorbox.close();
		if (jQuery(this).hasClass('fcsg-useasis')) {
			jQuery('form.cart .single_add_to_cart_button').trigger('click');
		} else {
			jQuery('.fw-update-cart').trigger('click');
		}
	});
	jQuery('a.zoom').colorbox({width:"85%"});
});

function fie_process_image_editor(fie_use) {
	if (fie_use == 1) {
		jQuery.colorbox({inline:true, href:'#fie-popup', maxWidth: '90%'});
	} else { // Always use image editor
		jQuery('.fie-popup .fie-popup-actions').hide();
		jQuery('.fie-popup .fie-popup-process').show();
		jQuery.colorbox({inline:true, href:'#fie-popup', maxWidth: '90%'});
		fie_goto_editor();
	}
}

function fie_goto_editor() {
	jQuery('.fw-update-cart').prop('disabled', true);
	window.location.href = fie_siteurl + '?fie_redirect=true&product_id='+fie_product_id+'&variation_id='+fie_variation_id+'&gallery_item_id='+fie_gallery_item_id+'&fie_cpage='+fie_cpage+'&qty='+fie_quantity;
}

function fie_get_use(pid, fie_enabled) {
	var fuse = 0;
	if (fie_enabled.length) {
		for (var i=0; i<fie_enabled.length; i++) {
			var fadata = fie_enabled[i].split('|');
			if (fadata[0] == pid) {
				fuse = parseInt(fadata[1]);
			}
		}
	}
	return fuse;
}
