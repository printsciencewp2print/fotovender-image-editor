<?php
/*
Plugin Name: Fotovender Image Editor
Description: Plugin allows to edit photo via API.
Version: 1.0.18
Author: Print Science
Bitbucket Plugin URI: printsciencewp2print/fotovender-image-editor
*/

if (!defined('ABSPATH')) exit;

@session_start();

define('FIE_DIR', dirname(__FILE__));
define('FIE_URL', plugins_url('/', __FILE__));

include(FIE_DIR . '/includes/actions.php');
include(FIE_DIR . '/includes/functions.php');
include(FIE_DIR . '/includes/admin.php');
include(FIE_DIR . '/includes/settings.php');
include(FIE_DIR . '/includes/woocommerce.php');

$fie_settings = get_option('fie_settings');

// plugin translation
add_action('plugins_loaded', 'fie_load_textdomain');
function fie_load_textdomain() {
	load_plugin_textdomain('fotovender-image-editor', false, plugin_basename(FIE_DIR) . '/languages'); 
}

// footer html
add_action('wp_footer', 'fie_wp_footer', 20);
function fie_wp_footer() {
	fie_osx_safari_message();
	fie_footer_html();
}

add_action('wp_enqueue_scripts', 'fie_wp_enqueue_scripts');
function fie_wp_enqueue_scripts() {
	wp_enqueue_script('fie-colorbox', FIE_URL . '/assets/js/jquery.colorbox.min.js', array( 'jquery' ), '1.3.4', true);
	wp_enqueue_style('fie-styles', FIE_URL . 'assets/css/style.css', array(), time());
	wp_enqueue_style('fie-colorbox-styles', FIE_URL . 'assets/css/colorbox.css', array(), time());
}
?>