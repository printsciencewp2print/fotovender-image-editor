<?php
// Add tabs to woocommerce product edit page
add_action('woocommerce_product_write_panel_tabs', 'fie_woocommerce_product_write_panel_tabs');
function fie_woocommerce_product_write_panel_tabs() {
	if (!fie_is_wp2print()) { ?>
	    <li class="fie_tab"><a href="#fie_options"><span><?php _e('Image Editor', 'fotovender-image-editor'); ?></span></a></li>
		<?php
	}
}

add_action('woocommerce_product_data_panels', 'fie_woocommerce_product_data_panels');
function fie_woocommerce_product_data_panels() {
    global $post;
	$fie_use = get_post_meta($post->ID, '_fie_use', true);
	$fie_product_id = get_post_meta($post->ID, '_fie_product_id', true);
	$fie_use_options = array(0 => __('Do not use image editor', 'fotovender-image-editor'), 1 => __('Option to use image editor', 'fotovender-image-editor'), 2 => __('Always use image editor', 'fotovender-image-editor'));
	if (!fie_is_wp2print()) { ?>
		<div id="fie_options" class="panel woocommerce_options_panel">
			<div class="options_group">
				<p class="form-field">
					<label><?php _e('Use Image Editor', 'fotovender-image-editor'); ?>:</label>
					<select name="fie_use" onchange="fie_use_action();">
						<?php foreach($fie_use_options as $okey => $oval) { ?>
						<option value="<?php echo $okey; ?>"<?php if ($okey == $fie_use) { echo ' SELECTED'; } ?>><?php echo $oval; ?></option>
						<?php } ?>
					</select>
				</p>
				<p class="form-field fie-product-id" style="display:none;">
					<label><?php _e('Image Editor Product ID', 'fotovender-image-editor'); ?>:</label>
					<input type="text" name="fie_product_id" value="<?php echo $fie_product_id; ?>">
				</p>
			</div>
		</div>
		<script>
		jQuery(document).ready(function() {
			fie_use_action();
		});
		function fie_use_action() {
			var fie_use = parseInt(jQuery('#fie_options select').val());
			if (fie_use > 0) {
				jQuery('#fie_options .fie-product-id').slideDown();
			} else {
				jQuery('#fie_options .fie-product-id').slideUp(100);
			}
		}
		</script>
		<?php
	}
}

add_action('woocommerce_process_product_meta', 'fie_woocommerce_process_product_meta');
function fie_woocommerce_process_product_meta($post_id) {
	if (!fie_is_wp2print()) {
		update_post_meta($post_id, '_fie_use', (int)$_POST['fie_use']);
		update_post_meta($post_id, '_fie_product_id', $_POST['fie_product_id']);
	}
}
?>