<?php
function fie_get_api_url($api_data) {
	global $fie_settings;
	$api_url = '';
	if ($fie_settings) {
		$token = fie_get_token($fie_settings);
		$s3_keys = fie_get_s3_keys();
		$session_id = fie_get_session_id();
		$success_url = site_url('/?'.implode('&', $api_data['success_params']));
		$cancel_url = get_permalink($api_data['cpage']);
		$locale = explode('_', get_locale());
		$ismobile = wp_is_mobile() ? 'TRUE' : 'FALSE';
		if ($token) {
			$parameters = array(
				'jwtToken='.$token,
				'sessionId='.$session_id,
				'client-product-id='.$api_data['product_id'],
				'productId='.$api_data['api_product_id'],
				'initial-params='.$api_data['initial_params'],
				'unit='.fie_get_value('unit', $api_data),
				'width='.fie_get_value('width', $api_data),
				'height='.fie_get_value('height', $api_data),
				'depth='.fie_get_value('depth', $api_data),
				'inputHR='.$api_data['input_image'],
				'outputHR='.$api_data['output_image'],
				'pingbackURL='.urlencode(site_url('/?fieapi=pingback')),
				'redirectUrl='.urlencode($success_url),
				'cancelUrl='.urlencode($cancel_url),
				'errorUrl='.urlencode($cancel_url),
				'ismobile='.$ismobile
			);
			if (isset($fie_settings['api_url'])) {
				$api_url = $fie_settings['api_url'];
				if (strpos($api_url, '?')) { $api_url .= '&'; } else { $api_url .= '?'; }
				$api_url .= 'lang='.$locale[0].'&request='.base64_encode(implode('&', $parameters));
			}
		} else {
			wp_die(__('Image Editor Token error. Please check Token Auth credentials.', 'fotovender-image-editor'));
		}
	}
	return $api_url;
}

function fie_get_session_id() {
	return md5(time());
}

function fie_get_s3_keys() {
	$s3_keys = array('access_key' => '', 'secret_key' => '', 'bucket' => '', 'region' => '');
	$fcsg_settings = get_option('fcsg_settings');
	if ($fcsg_settings) {
		$s3_keys['access_key'] = $fcsg_settings['s3_access_key'];
		$s3_keys['secret_key'] = $fcsg_settings['s3_secret_key'];
		$s3_keys['bucket'] = $fcsg_settings['s3_bucket'];
		$s3_keys['region'] = $fcsg_settings['s3_region'];
	}
	return $s3_keys;
}

function fie_get_success_url($api_data) {
	$params = array(
		'fie_api_action='.$api_data['fie_api_action'],
		'add-to-cart='.$api_data['product_id'],
		'product_id='.$api_data['product_id'],
		'variation_id='.$api_data['variation_id'],
		'quantity='.$api_data['qty']
	);
	return site_url('/?'.implode('&', $params));
}

function fie_get_token($fie_settings) {
	if (isset($fie_settings['token_url']) && isset($fie_settings['token_email']) && isset($fie_settings['token_pass'])) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $fie_settings['token_url'], // https://sails.fotovender.com/api/v1/user/login
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS =>'{"emailAddress":"'.$fie_settings['token_email'].'", "password":"'.$fie_settings['token_pass'].'"}', // ashish@9owls.com | ashish123
			CURLOPT_HTTPHEADER => array('Content-Type: application/json')
		));
		$response = curl_exec($curl);
		curl_close($curl);
		if ($response) {
			$result = json_decode($response);
			return $result->token;
		}
	}
	return false;
}

function fie_get_value($fkey, $fdata) {
	if (isset($fdata[$fkey])) {
		return $fdata[$fkey];
	}
	return '';
}

function fie_get_sessions() {
	return get_option('fie_sessions');
}

function fie_get_session_data($session_id) {
	$fie_sessions = fie_get_sessions();
	if (isset($fie_sessions[$session_id])) {
		return $fie_sessions[$session_id];
	}
}

function fie_set_session_data($session_id, $data) {
	$fie_sessions = fie_get_sessions();
	if (!is_array($fie_sessions)) { $fie_sessions = array(); }
	$fie_sessions[$session_id] = $data;
	update_option('fie_sessions', $fie_sessions);
}

function fie_clear_session_data($session_id) {
	$fie_sessions = fie_get_sessions();
	if (isset($fie_sessions[$session_id])) {
		unset($fie_sessions[$session_id]);
	}
	update_option('fie_sessions', $fie_sessions);
}

function fie_get_amazon_url($image_url) {
	if (function_exists('fcsg_get_amazon_file_url')) {
		return fcsg_get_amazon_file_url($image_url);
	}
	return $image_url;
}

function fie_get_enabled_products() {
	global $wpdb;
	$enabled_products = array();
	$eposts = $wpdb->get_results(sprintf("SELECT * FROM %spostmeta WHERE meta_key = '_fie_use' AND meta_value != '0'", $wpdb->prefix));
	if ($eposts) {
		foreach($eposts as $epost) {
			$enabled_products[] = $epost->post_id.'|'.$epost->meta_value;
		}
	}
	return $enabled_products;
}

function fie_footer_html() {
	global $post;
	$enabled_products = fie_get_enabled_products();
	?>
	<script type="text/javascript">
	var fie_siteurl = '<?php echo site_url(); ?>';
	var fie_cpage = '<?php if ($post) { echo $post->ID; } ?>';
	var fie_enabled = [];
	var fie_quantity = 0;
	var fie_product_id = 0;
	var fie_variation_id = 0;
	var fie_gallery_item_id = 0;
	<?php foreach($enabled_products as $akey => $aval) { ?>fie_enabled[<?php echo $akey; ?>] = '<?php echo $aval; ?>';<?php } ?>
	</script>
	<script type="text/javascript" src="<?php echo FIE_URL; ?>assets/js/scripts.js?ver=<?php echo time(); ?>"></script>
	<div style="display:none;">
		<div id="fie-popup" class="fie-popup">
			<div class="fie-popup-actions">
				<p><?php _e('You can use our image editor to adjust the size and colors of your image', 'fotovender-image-editor'); ?></p>
				<input type="button" class="button fie-launch" value="<?php _e('Launch image editor', 'fotovender-image-editor'); ?>">
				<input type="button" class="button fie-use-asis" value="<?php _e('Use image as-is', 'fotovender-image-editor'); ?>">
			</div>
			<div class="fie-popup-process"><?php _e('Launching image editor...', 'fotovender-image-editor'); ?></div>
		</div>
	</div>
	<?php
}

function fie_is_wp2print() {
	return function_exists('print_products_init');
}

function fie_is_osx_safari() {
	global $fie_settings;
	if ($fie_settings && isset($fie_settings['use_prevent']) && $fie_settings['use_prevent'] == 1) {
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		if (!wp_is_mobile() && stripos($user_agent, 'macintosh') !== false && stripos($user_agent, 'os x') !== false && stripos($user_agent, 'safari') !== false && stripos($user_agent, 'version/') !== false) {
			return true;
		}
	}
	return false;
}

function fie_osx_safari_message() {
	if (fie_is_osx_safari()) { ?>
		<div class="osx-safari-message"><?php _e('Our photo editor does not work well with the Safari browser. Please open this website using Chrome, Firefox or another browser.', 'fotovender-image-editor'); ?></div>
		<?php
	}
}
?>