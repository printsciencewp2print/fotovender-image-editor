<?php
add_action('wp_loaded', 'fie_settings_actions');
function fie_settings_actions() {
	if (isset($_POST['fie_admin_action']) && $_POST['fie_admin_action'] == 'save') {
		$fie_settings = $_POST['fie_settings'];
		update_option('fie_settings', $fie_settings);
		wp_redirect('options-general.php?page=fie-settings&updated=true');
		exit;
	}
	if (isset($_POST['fie_ajax_action']) && $_POST['fie_ajax_action'] == 'test-connection') {
		$settings = array(
			'token_url' => $_POST['token_url'],
			'token_email' => $_POST['token_email'],
			'token_pass' => $_POST['token_pass']
		);
		$token = fie_get_token($settings);
		if ($token) {
			echo 'OK';
		} else {
			echo 'ERROR';
		}
		exit;
	}
}

// admin settings
add_action('admin_menu', 'fie_admin_menu');
function fie_admin_menu() {
	add_options_page(__('Fotovender Image Editor', 'fotovender-image-editor'), __('Fotovender Image Editor', 'fotovender-image-editor'), 'manage_options', 'fie-settings', 'fie_admin_settings');
}

// admin settings page
function fie_admin_settings() {
	global $wpdb;
	$fie_settings = get_option('fie_settings');
	$wc_attributes = $wpdb->get_results(sprintf("SELECT * FROM %swoocommerce_attribute_taxonomies ORDER BY attribute_order, attribute_label", $wpdb->prefix));
	if (!isset($fie_settings['attributes'])) {
		$print_products_settings = get_option('print_products_settings');
		if (isset($print_products_settings['fotovender_attributes']) && $print_products_settings['fotovender_attributes']) {
			$fie_settings['attributes'] = unserialize($print_products_settings['fotovender_attributes']);
			update_option('fie_settings', $fie_settings);
		}
	}
	if (!is_array($fie_settings['attributes'])) { $fie_settings['attributes'] = array(); }
	?>
	<div class="wrap fie-settings-wrap">
		<h1><?php _e('Fotovender Image Editor Settings', 'fotovender-image-editor'); ?></h1>
		<form method="POST" class="fie-settings-form">
		<input type="hidden" name="fie_admin_action" value="save">
		<table style="width:auto;margin-top:10px;" cellspacing="8">
			<tr>
				<td colspan="2" style="font-weight:700;"><?php _e('Editor API', 'fotovender-image-editor'); ?>:</td>
			</tr>
			<tr>
				<td><?php _e('API URL', 'fotovender-image-editor'); ?>:</td>
				<td><input type="text" name="fie_settings[api_url]" value="<?php if (isset($fie_settings['api_url'])) { echo $fie_settings['api_url']; } ?>" style="width:400px;"></td>
			</tr>
			<tr>
				<td colspan="2" style="font-weight:700;"><br><?php _e('Token Auth Request', 'fotovender-image-editor'); ?>:</td>
			</tr>
			<tr>
				<td><?php _e('Request URL', 'fotovender-image-editor'); ?>:</td>
				<td><input type="text" name="fie_settings[token_url]" value="<?php if (isset($fie_settings['token_url'])) { echo $fie_settings['token_url']; } ?>" class="token-url" style="width:400px;"></td>
			</tr>
			<tr>
				<td><?php _e('Request Email', 'fotovender-image-editor'); ?>:</td>
				<td><input type="text" name="fie_settings[token_email]" value="<?php if (isset($fie_settings['token_email'])) { echo $fie_settings['token_email']; } ?>" class="token-email" style="width:400px;"></td>
			</tr>
			<tr>
				<td><?php _e('Request Password', 'fotovender-image-editor'); ?>:</td>
				<td><input type="password" name="fie_settings[token_pass]" value="<?php if (isset($fie_settings['token_pass'])) { echo $fie_settings['token_pass']; } ?>" class="token-pass" style="width:400px;"></td>
			</tr>
			<tr>
				<td colspan="2" style="font-weight:700;"><br><?php _e('Fotovender attributes', 'fotovender-image-editor'); ?>:</td>
			</tr>
			<tr>
				<td><?php _e('Attributes', 'fotovender-image-editor'); ?>:</td>
				<td>
					<?php if ($wc_attributes) {
						foreach($wc_attributes as $wc_attribute) { ?>
							<input type="checkbox" name="fie_settings[attributes][]" value="<?php echo $wc_attribute->attribute_id; ?>"<?php if (in_array($wc_attribute->attribute_id, $fie_settings['attributes'])) { echo ' CHECKED'; } ?>><?php echo $wc_attribute->attribute_label; ?><br>
							<?php
						}
					} ?>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="font-weight:700;"><br><?php _e('Additional options', 'fotovender-image-editor'); ?>:</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="checkbox" name="fie_settings[include_download]" value="1"<?php if (isset($fie_settings['include_download']) && $fie_settings['include_download'] == 1) { echo ' CHECKED'; } ?>><?php _e('Include download link to edited image in confirmation email', 'fotovender-image-editor'); ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="checkbox" name="fie_settings[use_prevent]" value="1"<?php if (isset($fie_settings['use_prevent']) && $fie_settings['use_prevent'] == 1) { echo ' CHECKED'; } ?>><?php _e('Prevent use by Safari Desktop user-agents', 'fotovender-image-editor'); ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" class="button-primary" value="<?php _e('Save Settings', 'fotovender-image-editor'); ?>" style="margin-top:10px;" /> <input type="button" class="button fie-test-connection" value="<?php _e('Test connection', 'fotovender-image-editor'); ?>" style="margin-top:10px;" /> <img src="<?php echo FIE_URL; ?>/assets/css/images/ajax-loading.gif" alt="" class="fie-tc-loading" style="display:none;">&nbsp;<span class="fie-tc-message fie-tc-success" style="color:#339900;display:none;"><?php _e('Connection is successfull.', 'fotovender-image-editor'); ?></span><span class="fie-tc-message fie-tc-error" style="color:#FF0000;display:none;"><?php _e('Connection is failed.', 'fotovender-image-editor'); ?></span></td>
			</tr>
		</table>
		</form>
	</div>
	<script>
	jQuery(document).ready(function(){
		jQuery('.fie-test-connection').click(function(){
			var turl = jQuery('.fie-settings-form .token-url').val();
			var temail = jQuery('.fie-settings-form .token-email').val();
			var tpass = jQuery('.fie-settings-form .token-pass').val();
			jQuery(this).prop('disabled', true);
			jQuery('.fie-tc-message').hide();
			jQuery('.fie-tc-loading').show();
			jQuery.post(
				'<?php echo admin_url('index.php'); ?>',
				{
					fie_ajax_action: 'test-connection',
					token_url: turl,
					token_email: temail,
					token_pass: tpass
				},
				function(data) {
					jQuery('.fie-test-connection').prop('disabled', false);
					jQuery('.fie-tc-loading').hide();
					if (data == 'OK') {
						jQuery('.fie-tc-success').show();
					} else {
						jQuery('.fie-tc-error').show();
					}
				}
			);
		});
	});
	</script>
	<?php
}

?>