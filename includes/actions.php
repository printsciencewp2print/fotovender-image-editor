<?php
add_action('wp_loaded', 'fie_actions');
function fie_actions() {
	if (isset($_REQUEST['fie_redirect']) && $_REQUEST['fie_redirect'] == 'true') {
		$product_id = $_GET['product_id'];
		$variation_id = $_GET['variation_id'];
		$gallery_item_id = $_GET['gallery_item_id'];
		$fie_cpage = $_GET['fie_cpage'];
		$qty = $_GET['qty'];

		if ($product_id && $gallery_item_id) {
			$fie_product_id = get_post_meta($product_id, '_fie_product_id', true);
			$image_attributes = wp_get_attachment_image_src($gallery_item_id, 'full');

			$image_path = substr($image_attributes[0], strpos($image_attributes[0], 'amazonaws.com/') + 14);
			$image_path = str_replace('/large/', '/original/', $image_path);

			$output_path = str_replace('/original/', '/edited/', $image_path);
			$output_path = str_replace('/edited/', '/edited/'.time().'/', $output_path);

			$api_data = array(
				'product_id' => $product_id,
				'api_product_id' => $fie_product_id,
				'initial_params' => '',
				'cpage' => $fie_cpage,
				'input_image' => urlencode($image_path),
				'output_image' => urlencode($output_path),
				'success_params' => array(
					'fie_api_action=fie',
					'add-to-cart='.$product_id,
					'product_id='.$product_id,
					'variation_id='.$variation_id,
					'quantity='.$qty
				)
			);
			
			$api_url = fie_get_api_url($api_data);
			header('Location: '.$api_url);
			exit;
		}
	}
}
?>