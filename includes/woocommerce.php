<?php
add_filter('woocommerce_add_cart_item_data', 'fie_woocommerce_add_cart_item_data', 10, 3);
function fie_woocommerce_add_cart_item_data($cart_item_data, $product_id, $variation_id) {
	if (isset($_REQUEST['fie_api_action']) && $_REQUEST['fie_api_action'] == 'fie' && isset($_REQUEST['outputHR'])) {
		$cart_item_data['fie_ukey'] = md5(time());
		$cart_item_data['fie_data'] = array(
			'image' => $_REQUEST['outputHR'],
			'thumb' => $_REQUEST['outputTH']
		);
	}
	return $cart_item_data;
}

add_filter('woocommerce_cart_item_permalink', 'fie_woocommerce_cart_item_permalink', 10, 3);
function fie_woocommerce_cart_item_permalink($permalink, $cart_item, $cart_item_key) {
	if (isset($cart_item['fie_data'])) {
		return false;
		if (isset($cart_item['fie_data']['image'])) {
			$permalink = fie_get_amazon_url($cart_item['fie_data']['image']);
		}
	}
	return $permalink;
}

add_filter('woocommerce_cart_item_thumbnail', 'fie_woocommerce_cart_item_thumbnail', 10, 3);
function fie_woocommerce_cart_item_thumbnail($image, $cart_item, $cart_item_key) {
	if (isset($cart_item['fie_data'])) {
		if (isset($cart_item['fie_data']['thumb'])) {
			$image = '<a href="'.fie_get_amazon_url($cart_item['fie_data']['image']).'" class="zoom"><img src="'.fie_get_amazon_url($cart_item['fie_data']['thumb']).'" alt=""></a>';
		}
	}
	return $image;
}

add_action('woocommerce_add_order_item_meta', 'fie_woocommerce_add_order_item_meta', 11, 3);
function fie_woocommerce_add_order_item_meta($item_id, $values, $cart_item_key) {
	$fie_data = false;
	if (isset($values['fie_data'])) {
		$fie_data = $values['fie_data'];
	} else if (isset($values['ieditor'])) {
		$fie_data = $values['ieditor'];
	}
	if ($fie_data) {
		wc_add_order_item_meta($item_id, '_fie_data', $fie_data);
	}
}

add_filter('woocommerce_hidden_order_itemmeta', 'fie_woocommerce_hidden_order_itemmeta');
function fie_woocommerce_hidden_order_itemmeta($metakeys) {
	$metakeys[] = '_fie_data';
	return $metakeys;
}

add_filter('woocommerce_admin_order_item_thumbnail', 'fie_woocommerce_admin_order_item_thumbnail', 10, 3);
function fie_woocommerce_admin_order_item_thumbnail($image, $item_id, $item) {
	$fie_data = wc_get_order_item_meta($item_id, '_fie_data', true);
	if ($fie_data && isset($fie_data['thumb'])) {
		return '<a href="'.fie_get_amazon_url($fie_data['image']).'" target="_blank"><img src="'.fie_get_amazon_url($fie_data['thumb']).'"></a>';
	}
	return $image;
}

$fie_email_order_details = false;
add_action('woocommerce_email_order_details', 'fie_woocommerce_email_order_details', 10);
function fie_woocommerce_email_order_details($order) {
	global $fie_email_order_details;
	$fie_email_order_details = true;
}

add_action('woocommerce_order_item_meta_end', 'fie_woocommerce_order_item_meta_end', 20, 2);
function fie_woocommerce_order_item_meta_end($item_id, $item) {
	global $fie_settings, $fie_email_order_details;
	if ($fie_email_order_details) {
		$fie_data = wc_get_order_item_meta($item_id, '_fie_data', true);
		if ($fie_data && isset($fie_data['thumb'])) {
			echo '<p><img src="'.fie_get_amazon_url($fie_data['thumb']).'" alt=""></p>';
		}
		if ($fie_data && isset($fie_data['image']) && isset($fie_settings['include_download']) && $fie_settings['include_download'] == 1) {
			echo '<p><a href="'.fie_get_amazon_url($fie_data['image']).'" target="_blank">'.__('Download image', 'fotovender-image-editor').'</a></p>';
		}
	}
}
?>